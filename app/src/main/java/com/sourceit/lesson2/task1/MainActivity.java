package com.sourceit.lesson2.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Deque;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    final private static int MAX_ARRAY_SIZE = 5;

    EditText text;
    TextView textError;
    TextView arrayIsEmptyError;
    Button eraseText;
    Button returnText;
    LinkedList<String> arrayOfTexts = new LinkedList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.input_text);
        eraseText = findViewById(R.id.button_erase_text);
        returnText = findViewById(R.id.button_return_text);
        textError = findViewById(R.id.input_text_error);
        arrayIsEmptyError = findViewById(R.id.empty_array);

        eraseText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String inputText = text.getText().toString();
                if (!checkErrorByEmptyField() && isOverArraySize(arrayOfTexts.size())) {
                    arrayOfTexts.add(inputText);
                }
                else if (!isOverArraySize(arrayOfTexts.size())) {
                    arrayOfTexts.add(inputText);
                    arrayOfTexts.removeFirst();
                }
                text.setText("");
            }
            private boolean checkErrorByEmptyField() {
                boolean hasError = false;
                if (text.getText().toString().isEmpty()) {
                    hasError = true;
                    textError.setVisibility(View.VISIBLE);
                } else {
                    textError.setVisibility(View.GONE);
                }
                return hasError;
            }
        });

        returnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkByEmptyArray(arrayOfTexts.isEmpty())) {
                    text.setText(arrayOfTexts.removeLast());
                }
            }
        });
    }

    private boolean checkByEmptyArray(boolean empty) {
        boolean hasError = false;
        if (arrayOfTexts.isEmpty()) {
            hasError = true;
            arrayIsEmptyError.setVisibility(View.VISIBLE);
        } else {
            arrayIsEmptyError.setVisibility(View.GONE);
        }
        return hasError;
    }

    private boolean isOverArraySize(int size) {
        return size < MAX_ARRAY_SIZE;
    }
}